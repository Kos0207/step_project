let names = document.querySelectorAll('.tabs-services-menu');
names.forEach(name => name.addEventListener('click', function () {
	const tab = name.dataset.tab
	console.log(tab)
	document.querySelector(".tabs-services-menu.active").classList.remove("active")
	name.classList.add("active")

	document.querySelector(".services-menu-content li.active").classList.remove("active")
	document.getElementById(tab).classList.add("active")
}));

let filters = document.querySelectorAll('.works-menu li');
filters.forEach(filter => filter.addEventListener('click', function () {
	const item = filter.dataset.filter
	console.log(item)
	document.querySelector(".works-menu li.active").classList.remove("active")
	filter.classList.add("active")

	document.querySelectorAll(".works-foto li").forEach(element => {
		if (item === "all") {
			element.classList.remove("hidden")
		} else if (element.classList.contains(item)) {
			element.classList.remove("hidden")
		} else {
			element.classList.add("hidden")
		}
		
		
	}); 
}));

 document.querySelector(".load-more .button").addEventListener("click", function (e) {
	e.preventDefault()
document.querySelector(".load-more-loading").classList.remove("spinner")
setTimeout(() => {
	document.querySelector(".load-more-loading").classList.add("spinner")
let notLoaded = document.querySelectorAll(".works-foto .not-loaded")
	if (notLoaded.length <= 12) {
 		document.querySelector(".load-more").classList.add("hidden")
 		document.querySelector(".load-more-spinner").classList.add("load-more-spinner")
	}
	notLoaded.forEach((el, index) => {
 		if (index < 12) {
			el.classList.remove("not-loaded")
		}
	})
	
 	}, 2000);
 })

var swiper = new Swiper(".mySwiper", {
	spaceBetween: 38,
	slidesPerView: 4,
	freeMode: true,
	watchSlidesProgress: true,
	navigation: {
		nextEl: ".swiper-button-next",
		prevEl: ".swiper-button-prev",
	  },
  });
  var swiper2 = new Swiper(".mySwiper2", {
	spaceBetween: 10,
	
	thumbs: {
	  swiper: swiper,
	},
  });




